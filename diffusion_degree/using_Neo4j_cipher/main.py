'''
this script reads a neo4j graph from server 52.28.5.51
extracts nodes matching authors type.
Then for each author, diffusion degree is evaluated.
Returns: csv file containing [author name, diffusion_degree, e-KTL_diffusion_score]
'''

import time
start_time = time.time()

from py2neo import Graph, Node, Relationship
import access_graph as diff
import analysis
import json
import math
import csv 

graph = Graph("http://localhost:7475", user="neo4j", password="neo4j", bolt_port=7688, bolt=True)
csvfile = "diffusion_neo4j_publications.csv"
print time.time() - start_time
# remove limit here
authors = list(graph.run("match (n:Authors) return n limit 2"))

with open(csvfile, 'wb') as outputf:
	fieldnames = ['author_name', 'diffusion value', 'diffusion slope']
	writer = csv.DictWriter(outputf, fieldnames=fieldnames)
   	writer.writeheader() 
	for author_i in authors: 
		author = author_i[0]
		# print author
		years = diff.timeline(author, graph)
		degrees = []
		t = time.time()
		for year in years:
			degrees.append(diff.diffusion(graph, author, year, 'Colorectal_Cancer'))
			#print time.time() - t
			t = time.time()
			#print year #, " : ", degree 
		diffusion_degree = degrees[len(degrees)-1]
		slope = analysis.regression(years, degrees)
		#slope = 1
		print diffusion_degree
		writer.writerow({'author_name':author["Author_Name"], 'diffusion value': diffusion_degree, 'diffusion slope': slope})

print time.time() - start_time
