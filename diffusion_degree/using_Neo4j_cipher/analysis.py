'''
this script run linear regression on x,y input
'''
from sklearn.linear_model import LinearRegression
import numpy


# normalizing using min-max scaling
def normalizing(ar):
        mean = numpy.mean(ar)
        ran = max(ar) - min(ar)
        nor_ar = (ar-mean)/ran
        return nor_ar, mean, ran


# returning slope after fitting x and y using linear regression
def regression(x, y, show_plot):
	x,y = numpy.array(x),numpy.array(y)
	x = x.reshape(-1, 1)
	y = y.reshape(-1, 1)
	x, x_mean, x_ran = normalizing(x)
	y, y_mean, y_ran = normalizing(y)
	if y_ran != 0:
		linear_regression = LinearRegression()
		linear_regression.fit(x,y)
		coef = linear_regression.coef_
		# inter = linear_regression.intercept_
		if show_plot:
			prediction = linear_regression.predict(x)
			pyplot.scatter(x, y)
			pyplot.plot(x,prediction)
			pyplot.show()
		return coef[0][0]
	else:
		return 0
