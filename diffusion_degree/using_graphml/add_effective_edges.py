'''
This script adds an effective edge between each pair of connected KTLs, whose weight
is equal to aggregated effective weight of all the relations between the KTLs.
Here, graph used is created on Rexter on local server
using bulbs to make graph queries
'''

# from bulbs.rexster import RexsterClient
from bulbs.config import Config
from bulbs.rexster import Graph

config = Config("http://localhost:7182/graphs/nikhil")
graph = Graph(config)

relations = ["publication_coauthor", "clinicalTrial_coauthor", "congress_coauthor", "patent_coauthor",
             "society_coauthor", "guideLine_coauthor", "regularity_coauthor"]
weights = dict({'publication_coauthor':0.14, 'clinicalTrial_coauthor':0.13, 'congress_coauthor':0.1, 'patent_coauthor':0.38, 'society_coauthor':0.03, 'guideLine_coauthor':0.30, 'regularity_coauthor':0.01})

vertices = graph.gremlin.query("g.V[0..100]")
script1 = "g.V.has('id', vertex_id).bothE.has('label',relations).as('x').bothV.has('id',index).back('x')"
if vertices is not None:
    for vertex in vertices:
        fromauthor = vertex._id
        if fromauthor % 1000 == 0:
            print fromauthor
        if vertex.element_type == "Author":
            script2 = 'g.v(id).bothE.has("label",T.in,["publication_coauthor", "clinicalTrial_coauthor", "congress_coauthor", "patent_coauthor", "society_coauthor", "guideLine_coauthor", "regularity_coauthor"]).bothV()'
            params = dict({'id': fromauthor})
            connected_v = graph.gremlin.query(script2, params)
            if connected_v is not None:
                # count = 0
                for v in connected_v:
                    if vertex != v:
                        effective_weight = 0
                        toauthor = long(v._id)
                        script1="g.V.has('id', "+str(fromauthor)+"L).bothE.has('label',T.in,"+str(relations)+").as('x').bothV.has('id',"+str(toauthor)+"L).back('x')"
                        edges=graph.gremlin.query(script1)
                        if edges is not None:
                            for edge in edges:
                                if not edge.get("effective_edge"):
                                    if weights.has_key(edge.label()):
                                        effective_weight += weights.get(edge.label()) * edge.get("weight")
                                        # print toauthor, fromauthor, effective_weight, edge.label(), edge.get("weight")
                            new_edge = graph.edges.create(vertex, "effective_edge", v)
                            new_edge.weight = effective_weight
                            new_edge.save()
                            # print effective_weight
                        # count += 1
                # print count
