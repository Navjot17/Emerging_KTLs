import igraph
import diffusion_cal as diff
import csv

input_file = "powermap_v3.graphml"
diffusion_values_tp = "diffusion_values_tp.csv"

# importing graph from graphml file and converting it into directed graph
g = igraph.Graph()
output = g.Read_GraphML(input_file)
Graph = output.as_undirected(mode="each")
print(Graph.summary())

# extracting subsets from graph
authors = Graph.vs.select(element_type="Author")

# writing into csv file
count = 0
with open(diffusion_values_tp, 'wb') as csvfile:
    fieldnames = ['author_name', 'diffusion']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for author in authors:  # author => node
        author_id = int(author["id"])
        diffusion = diff.diffusion(author_id, Graph)
        writer.writerow({'author_name': author["authorname"], 'diffusion': diffusion})
        count += 1
        if count%100:
            print count

'''
indications = Graph.vs.select(element_type="Indication")
interventions = Graph.vs.select(element_type="Intervention")
asset_classes = Graph.vs.select(element_type="assetClass")

print len(authors)  # 130820
print len(indications)  # 51
print len(interventions)  # 14518
print len(asset_classes)  # 7
print("edge attributes: ", Graph.edge_attributes())
'''



