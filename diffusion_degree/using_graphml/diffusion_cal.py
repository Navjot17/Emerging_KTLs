import math
import igraph


# calculating diffusion degree of node
def diffusion(node, graph):
    degree = 0
    for u in graph.neighbors(node):
        lambda_uv = diffusion_prob(u, node, graph)
        if lambda_uv == 0:
            continue
        exp = 0
        for v in graph.neighbors(u):
            lambda_iv = diffusion_prob(v, u, graph)
            exp += lambda_iv
        degree += lambda_uv * (1 + exp)
    return degree


# assigning weights and then calculating diffusion degree
def diffusion_prob(v, u, graph):
    temp_id = graph.get_eid(v, u)
    weight = graph.es[temp_id]["weight"]
    if not math.isnan(float(weight)):
        # print(graph.es[temp_id])
        fun = sigma(1, 1, 1)
        return fun(weight)
    else:
        return 0


# sigmoid function with variable coefficients
def sigma(a, b, c):
    w = lambda x: a/(b + math.exp(~c * x))
    return w

'''


    neighbors = Graph.neighbors(author_id) # returns list of indices of node's neighbours
    for node in neighbors:
        temp_id = Graph.get_eid(author_id, node)
        weight = Graph.es[temp_id]["weight"]
        if weight != 'nan':
            print weight
            '''

