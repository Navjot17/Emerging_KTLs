"""This script finds publications emerging ktls
This script convert the vifor publications collection into the following format
{
    '_id':<object id>,
    'count':<cumulative count of clinical trials by an investigator>,
    'slope': <slope for the count of CTs per year per investigator>,
    'last_name' : [
        <list of all the full name aliases(not last name) of an investigator,
    ],
    'stats': [
        {
            'count': <count of number of CT's in a year',
            'year': <year referenced in above field>
        }
    ],
    'base_ids' : [
        <list of all the base id that this document has referenced to get the data>
    ],
    'affiliation':
        <list of all the affiliations>   # Calculated at a later point of time, see aff_indi_ct.py
    ],
    'indication': [
        <list of all the indications>   # Calculated at a later point of time, see aff_indi_ct.py
    ]
}
"""

__author__ = 'Dhruv Shah'

import re
from pymongo import MongoClient
from pprint import pprint
import sys
from pprint import pprint
#from slopes import Slope_utility



conn = MongoClient('localhost', 27017)
db = conn.KTL
collection = db.oilbird_updated_publications

collection1 = db.pub_new_data
collection2 = db.pub_new_data_aggr

#{ field: { $gt: value1, $lt: value2 } }
def transfer_data(collection, collection1):
    collection1.drop()
    #print("dropping")
    j = collection.find({'year_str': {"$gt" : 2005}},{'_id': 1, 'year_str':1,'authors': 1, 'affiliations': 1, 'tag_v1': 1, 'impact_factor': 1}, batch_size=10).limit(20000)
    count = 0
    #print("j is", j)
    for a in j:
        one = {'_id': a['_id']}
        one['authors'] = a['authors']
        if type(a['year_str']) is not int:
            continue
        one['year_str'] = a['year_str']
        one['affiliations'] = a['affiliations']
        if 'tag_v1' in a:
	    one['tag_v1'] = a['tag_v1']
        if 'impact_factor' in a:
            one['impact_factor'] = a['impact_factor']

        collection1.insert_one(one)
        count += 1
        if count % 1000 == 0:
            print count



def year_aggregation(collection):
    pub_new_data_final = db.pub_new_data_final
    pub_new_data_final.drop()
    agg = collection.aggregate([
        # {'$match': {'_id.name': {'$nin': ct_blacklist}}},
        {
            '$group': {
                '_id': '$_id.name',
                'count_score': {'$sum': '$authors.score'}
            }
        },
        {'$sort': {'count_score': -1}},
        {'$limit': 10}
    ])
    agg = collection.aggregate([
        # {'$match': {'_id.name': {'$nin': ct_blacklist}}},
        # {'$match': {'_id.year': {'$ne': 0}}},
        # {'$unwind': '$base_ids'},
        {'$unwind': '$authors'},
        {
            '$group': {
                '_id': {
                    'name': '$authors.author_name',
                    'year': '$year_str',
                },
                #'count': {'$sum': 1},
       		'count_score':{'$sum':'$authors.score'},
		'base_ids': {'$addToSet': '$_id'},
                # 'last_name': {'$addToSet': '$last_name'}
            }
        },
        {'$unwind': '$base_ids'},
        # {'$unwind': '$last_name'},
        {
            '$group': {
                '_id': '$_id.name',
                'stats': {
                    '$addToSet': {'year': '$_id.year', 'count_score': '$count_score'}
                },
                'count_score': {'$sum': '$count_score'},
                'base_ids': {'$addToSet': '$base_ids'},
                # 'last_name': {'$addToSet': '$last_name'}
            }
        },
        {'$sort': {'count_score': -1}}
    ], allowDiskUse=True)    
    for a in agg:
	pub_new_data_final.insert(a)

transfer_data(collection, collection1)
year_aggregation(collection1)

''' This is to extract the top 20 people from emerging KTL
conditions : slope has to be positive
sorted by papers published descending
'''
db.pub_new_data_final.aggregate([
    {'$match': {'slope': {'$gt': 0}}},
    {'$match': {'_id': {'$ne': ''}}},  # Ensuring blank names do not appear
    {'$sort': {'count_score': -1}},
 #   {'$limit': 20},
    {'$out': 'emerging_pub_sort_count'}
])

''' This is to extract the top 20 people from emerging KTL
conditions : papers published has to be greater than 20
sorted by slope descending
'''
db.pub_new_data_final.aggregate([
    {'$match': {'count_score': {'$gt': 0}}},
    {'$match': {'_id': {'$ne': ''}}},
    {'$sort': {'slope': -1}},
#    {'$limit': 20},
    {'$out': 'emerging_pub_sort_slope'}
])





