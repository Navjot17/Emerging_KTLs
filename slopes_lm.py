import numpy
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
import sklearn.preprocessing 
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures



class Slope_utility:

    # x = numpy.array()
    # y = numpy.array()
    slope1 = 0
    slope2 = 0
    author_name = ''
    #polynomial_features = sklearn.preprocessing.PolynomialFeatures(3)
    #qq = pipeline = Pipeline([("polynomial_features", polynomial_features)])

    def __init__(self, author_name):
        self.author_name = author_name

    def normalizing(self,ar):
        mean = numpy.mean(ar)
        ran = max(ar) - min(ar)
        nor_ar = (ar-mean)/ran

        return nor_ar, mean, ran

    def denormalizing(self,nor_ar, mean, ran):
        return nor_ar * ran + mean


    def drawplot(self,x, y, show_plot=False):
        x = x.reshape(-1,1)
        y = y.reshape(-1,1)
        x,x_mean,x_ran = self.normalizing(x)
        # y,y_mean,y_ran = self.normalizing(y)
        slope = self.poly_regression(x,y, show_plot)
        predict_pt_1 = (2017 - x_mean)/x_ran
        predict_pt_2 = (2018 - x_mean)/x_ran
       	#print("predict_pt_1: ", predict_pt_1, " predict_pt_2:", predict_pt_1) 
	slope_count = (slope(predict_pt_1)+slope(predict_pt_2))/2
        return slope_count


    def preprocessing(self,x,y):
        yx = zip(x,y)
        yx.sort()
        x,y = zip(*yx)
        x1,y1 = numpy.array(x),numpy.array(y)
        return x1,y1

    #TODO: Add condition to deal with degree
    #    Change the static degree parameter,
    #    Also change the formula for calculating slope from static to variable
    def poly_regression(self,x, y, show_plot):
	linear_regression = LinearRegression()
       	linear_regression.fit(x,y)
	coef = linear_regression.coef_#get_params(deep = True)
	inter = linear_regression.intercept_
	#print('linear equation: %f*x + %f' %(coef[0][0],inter[0]))
	prediction = linear_regression.predict(x)
	#print('predicted values using linear regression:', prediction)
	#slope = lambda x: inter[0] + coef[0][0]*x
	#print("predicted value of scores after 2 yrs using LM: %f" %slope_count[0])
	slope = lambda x: coef[0][0]
	#slope_count = (slope(predict_pt_1)+slope(predict_pt_2))/2
	#print("predicted value of growth after 2 yrs using LM: %f" %slope_count)
        if show_plot:
            pyplot.scatter(x, y)
            pyplot.plot(x,pipeline.predict(x))
            pyplot.savefig(self.author_name, format='png')
            pyplot.close()
	#slope = slope_count[0]
       # coef =  pipeline.named_steps['linear_regression'].coef_
	#slope = lambda x: coef[0][1] +2*coef[0][2]* x + 3*coef[0][3]* x**2
        return slope


